# Vee24DemoApp

This iOS app is used to showcase how Vee24's product can be implemented into prospects' mobile applications.

## Setup

When working with this Xcode project, use vee24DemoApp.xcworkspace. Do not use vee24DemoApp.xcodeproj. The .xcworkspace file is created after running 

'''bash
pod install
'''

after adding third party libraries to the Podfile.

## To-Do

List of next steps can be found on [this](https://docs.google.com/spreadsheets/d/1Bf3a45NAu75_H2tUSaU593VF8apgWRQclz-zaJqWEbQ/edit?usp=sharing) Google Spreadsheet.



