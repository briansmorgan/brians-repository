//
//  UIViewControllerExtension.swift
//  vee24DemoApp
//
//  Created by Brian Morgan on 7/20/20.
//  Copyright © 2020 Brian Morgan. All rights reserved.
//

import UIKit

struct UserData: Codable {
    let firstName: String
    let lastName: String
    let phoneNumber: String
    let homeAddress: String
    let emailAddress: String
    let cars: [CarData]
}

struct CarData: Codable {
    let modelName: String
    let modelYear: String
    let imageName: String
    let mileageAtLastService: String
    let averageMilesPerYear: String
}

//this data is used to intialize an empty globalUserData object
let samplejsondata = """
{
    "firstName": "",
    "lastName": "",
    "phoneNumber": "",
    "homeAddress": "",
    "emailAddress": "",
    "cars": [
        {
            "modelName": "",
            "modelYear": "",
            "imageName": "",
            "mileageAtLastService": "66,666",
            "averageMilesPerYear": "24"
        },
        {
            "modelName": "",
            "modelYear": "",
            "imageName": "",
            "mileageAtLastService": "",
            "averageMilesPerYear": ""
        }
    ]
}
""".data(using: .utf8)!
//Use the above sample JSON to fulfill requirement when initializing an instance of a Codable struct

var globalUserData = try? JSONDecoder().decode(UserData.self, from: samplejsondata) //statement to initialize an empty instance of type UserData

extension UIViewController {
    
    /// styles a chat button in the accordion menu
    /// - Parameters:
    ///   - button: one of the three chat buttons
    ///   - buttonTitle: string of what appears on button
    ///   - overlayedImageName: literal name of the image that appears on button. Image found in Assests.xcassets folder
    ///   - backgroundImageName: literal name of the image that appears on in background of button. Image found in Assests.xcassets folder
    func formatChatBtns(button: UIButton, buttonTitle: String, overlayedImageName: String, backgroundImageName: String) {
        button.contentHorizontalAlignment = .left //fixes button title to left of button
        button.contentVerticalAlignment = .bottom //fixes button title to bottom of button
        button.setTitle(buttonTitle, for: .normal)
        button.titleLabel?.font = UIFont(name: "Noto Sans Myanmar", size: 24)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 50, bottom: 12, right: 0) //positions title within button
        button.setTitleColor(.black, for: .normal)
        let overlayedImage = UIImage(named: overlayedImageName) as UIImage?
        button.setImage(overlayedImage, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 40, bottom: 29, right: 0)
        let image = UIImage(named: backgroundImageName) as UIImage?
        button.setBackgroundImage(image, for: .normal)
        button.tintColor = UIColor.black
        button.alpha = 1
        button.backgroundColor = .white
    }
    
    //function necessary for JSON parsing
    public func readLocalFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name, ofType: "json"),
                let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        return nil
    }
    
    //function necessary for JSON parsing
    func parse(jsonData: Data) -> UserData? {
        do {
            let decodedData = try JSONDecoder().decode(UserData.self, from: jsonData)
            return decodedData
            
        } catch {
            print("decode error")
            return nil
        }
    }
    
}
