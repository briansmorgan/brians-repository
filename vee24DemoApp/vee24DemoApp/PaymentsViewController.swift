//
//  PaymentsViewController.swift
//  vee24DemoApp
//
//  Created by Brian Morgan on 7/21/20.
//  Copyright © 2020 Brian Morgan. All rights reserved.
//

import UIKit


class PaymentsViewController: UIViewController {

    @IBOutlet var chatOptionsBtns: [UIButton]!
    @IBOutlet weak var openAccordionBtn: UIButton!
    @IBOutlet weak var videoChatBtn: UIButton!
    @IBOutlet weak var textChatBtn: UIButton!
    @IBOutlet weak var audioChatBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        formatChatBtns(button: textChatBtn, buttonTitle: "Text Chat", overlayedImageName: "chatBubble", backgroundImageName: "textChatVisual")
        formatChatBtns(button: audioChatBtn, buttonTitle: "Audio Chat", overlayedImageName: "microphone", backgroundImageName: "audioChatVisual")
        formatChatBtns(button: videoChatBtn, buttonTitle: "Video Chat", overlayedImageName: "camera", backgroundImageName: "videoChatVisual")
        
        let upArrowImage = UIImage(named: "upTick")
        openAccordionBtn.setImage(upArrowImage, for: .normal)
        openAccordionBtn.tintColor = UIColor.gray
        
    }

    @IBAction func chatOptionsBtnTapped(_ sender: UIButton) {
    chatOptionsBtns.forEach { (button) in
        UIView.animate(withDuration: 0.3) {
            button.isHidden = !button.isHidden
            self.view.layoutIfNeeded()
            }
        }
    }
}
