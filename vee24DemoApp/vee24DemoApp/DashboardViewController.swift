//
//  DashboardViewController.swift
//  vee24DemoApp
//
//  Created by Brian Morgan on 7/21/20.
//  Copyright © 2020 Brian Morgan. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    //objects declared using the GUI
    @IBOutlet var chatOptionsBtns: [UIButton]!
    @IBOutlet weak var openAccordionBtn: UIButton!
    @IBOutlet weak var videoChatBtn: UIButton!
    @IBOutlet weak var textChatBtn: UIButton!
    @IBOutlet weak var audioChatBtn: UIButton!
    @IBOutlet weak var dashboardTab: UITabBarItem!
    let topBarView = UIView()
    let topSpaceFillerView = UIView()
    let logoImageView = UIImageView(image: UIImage(named: "nem-logo"))
    let mainCarCard = UIView()
    let currentCarImageView = UIImageView()
    let mainCarLabel = UILabel()
    let mileageAtLastServiceLabel = UILabel()
    let averageMilesPerYearLabel = UILabel()
    let dashboardScrollView = UIScrollView()
    let extraInfoCarCard = UIView()
    let serviceHistoryLabel = UILabel()
    let extraInfoStackView = UIStackView()

    var item1: UIView {
        return makeItemInStackView(imageName: "wrenchGraphic", labelText: "NEM Service Center", date: "5/5/2020")
    }
    var item2: UIView {
        return makeItemInStackView(imageName: "oilGraphic", labelText: "Drain and refill engine oil", date: "3/1/2020")
    }
    var item3: UIView {
        return makeItemInStackView(imageName: "carGraphic", labelText: "Tire Rotation", date: "1/21/20")
    }
    
    fileprivate func makeTopBar() { //function that formats and styles topBar
        topBarView.translatesAutoresizingMaskIntoConstraints = false
        topBarView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        topBarView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        topBarView.heightAnchor.constraint(equalToConstant: 45).isActive = true
        topBarView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        
        topSpaceFillerView.translatesAutoresizingMaskIntoConstraints = false
        topSpaceFillerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        topSpaceFillerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        topSpaceFillerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        topSpaceFillerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        topBarView.backgroundColor = #colorLiteral(red: 0.2097159028, green: 0.3459104896, blue: 0.4652820826, alpha: 1)
        topSpaceFillerView.backgroundColor = #colorLiteral(red: 0.2097159028, green: 0.3459104896, blue: 0.4652820826, alpha: 1)
        
        logoImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: logoImageView, attribute: .leftMargin, relatedBy: .equal, toItem: topBarView, attribute: .left, multiplier: 1, constant: 20).isActive = true
        NSLayoutConstraint(item: logoImageView, attribute: .bottomMargin, relatedBy: .equal, toItem: topBarView, attribute: .bottomMargin, multiplier: 1, constant: -4).isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                    
        let carsArray = globalUserData?.cars //array of car data from JSON
        formatAllAccordionMenuBtns() //styles accordion menu
        
        //sets constraints for dashboardScrollView
        view.addSubview(dashboardScrollView)
        dashboardScrollView.translatesAutoresizingMaskIntoConstraints = false
        dashboardScrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        dashboardScrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 45).isActive = true
        dashboardScrollView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor).isActive = true
        dashboardScrollView.bottomAnchor.constraint(equalTo: openAccordionBtn.topAnchor).isActive = true
        dashboardScrollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height)
        
        //creates view hierarchy. objects that should scroll are added to dashboardScrollView
        view.addSubview(topSpaceFillerView)
        view.addSubview(topBarView)
        view.addSubview(logoImageView)
        dashboardScrollView.addSubview(mainCarCard)
        dashboardScrollView.addSubview(currentCarImageView)
        dashboardScrollView.addSubview(mainCarLabel)
        dashboardScrollView.addSubview(mileageAtLastServiceLabel)
        dashboardScrollView.addSubview(averageMilesPerYearLabel)
        dashboardScrollView.addSubview(extraInfoCarCard)
        extraInfoCarCard.addSubview(serviceHistoryLabel)
        extraInfoCarCard.addSubview(extraInfoStackView)
        
        makeTopBar() //programmatically generates the top bar with the NEM logo
        let mainCarCardHeight = view.frame.height/4
        
        mainCarCard.translatesAutoresizingMaskIntoConstraints = false
        mainCarCard.centerXAnchor.constraint(equalTo: dashboardScrollView.centerXAnchor).isActive = true
        mainCarCard.leadingAnchor.constraint(equalTo: dashboardScrollView.leadingAnchor, constant: 10).isActive = true
        mainCarCard.heightAnchor.constraint(equalToConstant: mainCarCardHeight).isActive = true
        mainCarCard.topAnchor.constraint(equalTo: dashboardScrollView.topAnchor, constant: 10).isActive = true
        
        //sets constraints for currentCarImageView--the Image on the top of the screen
        currentCarImageView.translatesAutoresizingMaskIntoConstraints = false
        currentCarImageView.centerXAnchor.constraint(equalTo: mainCarCard.centerXAnchor).isActive = true
        NSLayoutConstraint(item: currentCarImageView, attribute: .bottomMargin, relatedBy: .equal, toItem: mainCarCard, attribute: .bottomMargin, multiplier: 1, constant: -(mainCarCardHeight/4)).isActive = true
        currentCarImageView.image = UIImage(named: "libraImage") //temporary image just to show that this image view exists
        
        //sets constraints for mainCarLabel--the label of the car at the top of the screen
        mainCarLabel.translatesAutoresizingMaskIntoConstraints = false
        mainCarLabel.text = "\((carsArray![0].modelYear) + " " + carsArray![0].modelName)"
        NSLayoutConstraint(item: mainCarLabel, attribute: .topMargin, relatedBy: .equal, toItem: mainCarCard, attribute: .topMargin, multiplier: 1, constant: 4).isActive = true
        NSLayoutConstraint(item: mainCarLabel, attribute: .leftMargin, relatedBy: .equal, toItem: mainCarCard, attribute: .leftMargin, multiplier: 1, constant: 4).isActive = true
        
        //sets constraints for averageMilePerYearLabel
        averageMilesPerYearLabel.translatesAutoresizingMaskIntoConstraints = false
        averageMilesPerYearLabel.text = "Average miles per year: \(carsArray![0].averageMilesPerYear)"
        NSLayoutConstraint(item: averageMilesPerYearLabel, attribute: .bottomMargin, relatedBy: .equal, toItem: mainCarCard, attribute: .bottomMargin, multiplier: 1, constant: -4).isActive = true
        NSLayoutConstraint(item: averageMilesPerYearLabel, attribute: .leftMargin, relatedBy: .equal, toItem: mainCarCard, attribute: .leftMargin, multiplier: 1, constant: 4).isActive = true
        
        //sets constraints for mileageAtLastServiceLabel
        mileageAtLastServiceLabel.translatesAutoresizingMaskIntoConstraints = false
        mileageAtLastServiceLabel.text = "Mileage at last service: \(carsArray![0].mileageAtLastService)"
        mileageAtLastServiceLabel.leadingAnchor.constraint(equalTo: mainCarCard.leadingAnchor, constant: 4).isActive = true
        mileageAtLastServiceLabel.bottomAnchor.constraint(equalTo: averageMilesPerYearLabel.topAnchor, constant: 0).isActive = true
        
        //sets constraints for extraInfoCarCard
        extraInfoCarCard.translatesAutoresizingMaskIntoConstraints = false
        extraInfoCarCard.centerXAnchor.constraint(equalTo: dashboardScrollView.centerXAnchor).isActive = true
        extraInfoCarCard.leadingAnchor.constraint(equalTo: dashboardScrollView.leadingAnchor, constant: 10).isActive = true
        extraInfoCarCard.topAnchor.constraint(equalTo: mainCarCard.bottomAnchor, constant: 10).isActive = true
        extraInfoCarCard.heightAnchor.constraint(equalToConstant: view.frame.height/1.45).isActive = true //equal to view.frame.height/1.45 so that the extraInfoCarCard fits within the scrollView
        
        //sets constraints for serviceHistoryLabel
        serviceHistoryLabel.translatesAutoresizingMaskIntoConstraints = false
        serviceHistoryLabel.text = "Service History"
        serviceHistoryLabel.topAnchor.constraint(equalTo: extraInfoCarCard.topAnchor, constant: 10).isActive = true
        serviceHistoryLabel.leadingAnchor.constraint(equalTo: extraInfoCarCard.leadingAnchor, constant: 20).isActive = true
        serviceHistoryLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        //sets constraints for and formats extraInfoStackView. Needs to be made dynamic based on how previous service history
        extraInfoStackView.translatesAutoresizingMaskIntoConstraints = false
        extraInfoStackView.distribution = .fillEqually
        extraInfoStackView.alignment = .fill
        extraInfoStackView.topAnchor.constraint(equalTo: extraInfoCarCard.topAnchor, constant: 30).isActive = true
        extraInfoStackView.bottomAnchor.constraint(equalTo: extraInfoCarCard.bottomAnchor, constant: -15).isActive = true
        extraInfoStackView.axis = .vertical
        extraInfoStackView.spacing = 0
        extraInfoStackView.addArrangedSubview(item1)
        extraInfoStackView.addArrangedSubview(item2)
        extraInfoStackView.addArrangedSubview(item3)
        extraInfoStackView.centerXAnchor.constraint(equalTo: extraInfoCarCard.centerXAnchor).isActive = true
        extraInfoStackView.leadingAnchor.constraint(equalTo: extraInfoCarCard.leadingAnchor).isActive = true
        
        //styles mainCarCard and extra InfoCarCard
        mainCarCard.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        mainCarCard.layer.cornerRadius = 10
        mainCarCard.layer.shadowColor = UIColor.black.cgColor
        mainCarCard.layer.shadowRadius = 4
        mainCarCard.layer.shadowOpacity = 0.1
        extraInfoCarCard.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        extraInfoCarCard.layer.cornerRadius = 10
        extraInfoCarCard.layer.shadowColor = UIColor.black.cgColor
        extraInfoCarCard.layer.shadowRadius = 4
        extraInfoCarCard.layer.shadowOpacity = 0.1
    }
        
    @IBAction func chatOptionsBtnTapped(_ sender: UIButton) { //animates opening accordion menu
    chatOptionsBtns.forEach { (button) in
        UIView.animate(withDuration: 0.3) {
            button.isHidden = !button.isHidden
            self.view.layoutIfNeeded()
            }
        }
    }
    
    /// styles accordion menu
    fileprivate func formatAllAccordionMenuBtns() {
        formatChatBtns(button: textChatBtn, buttonTitle: "Text Chat", overlayedImageName: "chatBubble", backgroundImageName: "textChatVisual")
        formatChatBtns(button: audioChatBtn, buttonTitle: "Audio Chat", overlayedImageName: "microphone", backgroundImageName: "audioChatVisual")
        formatChatBtns(button: videoChatBtn, buttonTitle: "Video Chat", overlayedImageName: "camera", backgroundImageName: "videoChatVisual")
        
        let upArrowImage = UIImage(named: "upTick")
        openAccordionBtn.setImage(upArrowImage, for: .normal)
        openAccordionBtn.tintColor = UIColor.gray
    }
    
    /// makes an item in the stack view located in the extraInfoCarCard
    /// - Parameters:
    ///   - imageName: literal name of the image that appears on button. Image found in Assests.xcassets folder
    ///   - labelText: literal text that appears on the object
    ///   - date: literal date that appears on the object
    /// - Returns: a UIView that is inserted into the extraInfoStackView, which is apart of the extraInfoCarCard
    func makeItemInStackView(imageName: String, labelText: String, date: String) -> UIView {
        let v = UIView()
        let i = UIImageView(image: UIImage(named: imageName))
        let itemLabel = UILabel(); itemLabel.text = labelText
        let dateLabel = UILabel(); dateLabel.text = date; dateLabel.textAlignment = .center; dateLabel.textColor = .lightGray
        v.translatesAutoresizingMaskIntoConstraints = false
        i.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        itemLabel.translatesAutoresizingMaskIntoConstraints = false
        v.addSubview(i)
        v.addSubview(itemLabel)
        v.addSubview(dateLabel)
        v.layer.borderWidth = 1.5
        v.layer.borderColor = UIColor(red: 0.769, green: 0.769, blue: 0.769, alpha: 1).cgColor
        i.leadingAnchor.constraint(equalTo: v.leadingAnchor, constant: 10).isActive = true
        i.centerYAnchor.constraint(equalTo: v.centerYAnchor).isActive = true
        itemLabel.leadingAnchor.constraint(equalTo: i.trailingAnchor, constant: view.frame.width/6).isActive = true
        itemLabel.centerYAnchor.constraint(equalTo: v.centerYAnchor).isActive = true
        dateLabel.bottomAnchor.constraint(equalTo: i.topAnchor, constant: -15).isActive = true
        dateLabel.leadingAnchor.constraint(equalTo: i.leadingAnchor).isActive = true
        return v
    }
}
    
