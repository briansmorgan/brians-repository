//
//  ProfileViewController.swift
//  vee24DemoApp
//
//  Created by Brian Morgan on 6/7/20.
//  Copyright © 2020 Brian Morgan. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var yourCarsLabel: UILabel!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var userPhone: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userHomeAddress: UILabel!
    @IBOutlet var chatOptionsBtns: [UIButton]!
    @IBOutlet weak var openAccordionBtn: UIButton!
    @IBOutlet weak var videoChatBtn: UIButton!
    @IBOutlet weak var textChatBtn: UIButton!
    @IBOutlet weak var audioChatBtn: UIButton!
    @IBOutlet weak var accordionMenuStackView: UIStackView!
    
    var constraintContainer: [NSLayoutConstraint] = [] //used to activate constraints in bulk

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //ProfileViewController is intitial view controller.
        //Initialize globalUserData the first time ProfileViewController opens
        let localData = self.readLocalFile(forName: "UserDataFile")
        globalUserData = self.parse(jsonData: localData!)!
        let carsArray = globalUserData?.cars //
        
        let buttonHeight = view.frame.height/5
        let inBetweenButtonSpacing = buttonHeight/15
        let spacingForLabelOnButton = buttonHeight/20
        let moveImageUp = buttonHeight/10
        var px: CGFloat = 0 //variable used to dynamically space the car buttons
        
        for car in carsArray! { //loop that dynamically generates car cards
            let carCardBtn = UIButton()
            let carCardLabel = UILabel(); carCardLabel.text = "\(car.modelYear + " " + car.modelName)"
            let carImageView = UIImageView(); let carImage = UIImage(named: "\(car.imageName)"); carImageView.image = carImage
            self.view.addSubview(carCardLabel)
            self.view.addSubview(carCardBtn)
            self.view.addSubview(carImageView)
            view.bringSubviewToFront(carCardLabel)
            carCardBtn.backgroundColor = .white
            carCardBtn.layer.cornerRadius = 10
            carCardBtn.layer.shadowColor = UIColor.black.cgColor
            carCardBtn.layer.shadowRadius = 4
            carCardBtn.layer.shadowOpacity = 0.1
            carCardBtn.showsTouchWhenHighlighted = true
            
            //constraints for carCardBtn(s) and objects within carCardBtn
            carCardBtn.translatesAutoresizingMaskIntoConstraints = false
            let constraintButtonHeight = carCardBtn.heightAnchor.constraint(equalToConstant: buttonHeight)
            let constraintButtonToYourCarsLabel = NSLayoutConstraint(
                item: carCardBtn, attribute: .topMargin, relatedBy: .equal,
                toItem: yourCarsLabel, attribute: .bottomMargin, multiplier: 1, constant: CGFloat(30 + px)) //px variable used to space buttons in between one another
            let constraintButtonCenterHorizontally = carCardBtn.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
            let constraintStretchButtonsToEdges = carCardBtn.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10)
            
            carCardLabel.translatesAutoresizingMaskIntoConstraints = false
            let constraintLabelToTop = NSLayoutConstraint(item: carCardLabel, attribute: .topMargin, relatedBy: .equal, toItem: carCardBtn, attribute: .topMargin, multiplier: 1, constant: spacingForLabelOnButton)
            let constraintMoveLabelToRight = NSLayoutConstraint(item: carCardLabel, attribute: .leftMargin, relatedBy: .equal, toItem: carCardBtn, attribute: .leftMargin, multiplier: 1, constant: spacingForLabelOnButton)
            
            carImageView.translatesAutoresizingMaskIntoConstraints = false
            let constraintImageToBottomOfButton = NSLayoutConstraint(item: carImageView, attribute: .bottomMargin, relatedBy: .equal, toItem: carCardBtn, attribute: .bottomMargin, multiplier: 1, constant: -(moveImageUp))
            let constraintCenterImageInButton = carImageView.centerXAnchor.constraint(equalTo: carCardBtn.centerXAnchor)
            
            constraintContainer.append(constraintButtonCenterHorizontally)
            constraintContainer.append(constraintButtonToYourCarsLabel)
            constraintContainer.append(constraintStretchButtonsToEdges)
            constraintContainer.append(constraintButtonHeight)
            constraintContainer.append(constraintLabelToTop)
            constraintContainer.append(constraintMoveLabelToRight)
            constraintContainer.append(constraintImageToBottomOfButton)
            constraintContainer.append(constraintCenterImageInButton)
            
            px += buttonHeight + inBetweenButtonSpacing //ensures that next button is located farther down from previous button by adding the button height + the spacing in between the button to the following car card's constraint
        }
        
        NSLayoutConstraint.activate(constraintContainer) //activates constraints for the car cards loop
        
        welcomeLabel.text = "Welcome Back, \(globalUserData!.firstName)"
        userPhone.text = globalUserData!.phoneNumber
        userEmail.text = globalUserData!.emailAddress
        userHomeAddress.text = globalUserData!.homeAddress //these 4 lines
        
        formatAllAccordionMenuOptions() //styles the accordion menu
    }
    
    @IBAction func openChatOptionsBtnTapped(_ sender: UIButton) {
        chatOptionsBtns.forEach { (button) in
            UIView.animate(withDuration: 0.3) {
                button.isHidden = !button.isHidden
                self.view.layoutIfNeeded()
            }
        }
        view.bringSubviewToFront(accordionMenuStackView) //necessary so accordion menu does not slide up behind the car cards
    }
    
    func formatAllAccordionMenuOptions() {
        formatChatBtns(button: textChatBtn, buttonTitle: "Text Chat", overlayedImageName: "chatBubble", backgroundImageName: "textChatVisual")
        formatChatBtns(button: audioChatBtn, buttonTitle: "Audio Chat", overlayedImageName: "microphone", backgroundImageName: "audioChatVisual")
        formatChatBtns(button: videoChatBtn, buttonTitle: "Video Chat", overlayedImageName: "camera", backgroundImageName: "videoChatVisual")
        
        let upArrowImage = UIImage(named: "upTick")
        openAccordionBtn.setImage(upArrowImage, for: .normal)
        openAccordionBtn.tintColor = UIColor.gray
    }
    
    //functionality of tapping the video chat, audio chat, and text chat buttons
    @IBAction func videoChatBtnTapped(_ sender: Any) {
    }
    
    @IBAction func audioChatBtnTapped(_ sender: Any) {
    }
    
    @IBAction func textChatBtnTapped(_ sender: Any) {
    }
}
    

