//
//  ServicesViewController.swift
//  vee24DemoApp
//
//  Created by Brian Morgan on 7/21/20.
//  Copyright © 2020 Brian Morgan. All rights reserved.
//

import UIKit


class ServicesViewController: UIViewController {
    
    @IBOutlet var chatOptionsBtns: [UIButton]!
    @IBOutlet weak var openAccordionBtn: UIButton!
    @IBOutlet weak var videoChatBtn: UIButton!
    @IBOutlet weak var textChatBtn: UIButton!
    @IBOutlet weak var audioChatBtn: UIButton!
    @IBOutlet weak var topBar: UINavigationBar!
    let servicesScrollView = UIScrollView()
    let btnScheduleAppointment = UIButton()
    let upcomingAppointmentsView = UIView()
    let upcomingAppointmentsLabel = UILabel()
    let wrenchImage = UIImageView(image: UIImage(named: "wrenchGraphic"))
    let itemLabel = UILabel()
    let dateLabel = UILabel()
    let myStatsView = UIView()
    let myStatsLabel = UILabel()
    let statsStackView = UIStackView()
    let warningView = UIView(); let warningImageView = UIImageView(image: UIImage(named: "warningGraphic")); let warningTextLabel = UILabel() //warningView and its objects within it
    let safetyInspectionLabel = UILabel()
    
    var thereIsWarning = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        formatAccordionMenuBtns() //function that sets up accordion menu
        
        //set constraints for servicesScrollView
        view.addSubview(servicesScrollView)
        servicesScrollView.contentSize = CGSize(width: view.frame.width, height: (view.frame.height + 200))
        servicesScrollView.translatesAutoresizingMaskIntoConstraints = false
        servicesScrollView.topAnchor.constraint(equalTo: topBar.bottomAnchor).isActive = true
        servicesScrollView.bottomAnchor.constraint(equalTo: openAccordionBtn.topAnchor).isActive = true
        servicesScrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        servicesScrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        
        servicesScrollView.addSubview(btnScheduleAppointment) //set constraints and style for btnScheduleAppointment
        btnScheduleAppointment.translatesAutoresizingMaskIntoConstraints = false
        btnScheduleAppointment.layer.cornerRadius = 10
        btnScheduleAppointment.layer.backgroundColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        btnScheduleAppointment.layer.borderWidth = 2
        btnScheduleAppointment.layer.borderColor = UIColor.white.cgColor
        btnScheduleAppointment.setTitle("Schedule an Appointment", for: .normal)
        btnScheduleAppointment.heightAnchor.constraint(equalToConstant: 41).isActive = true
        btnScheduleAppointment.topAnchor.constraint(equalTo: servicesScrollView.topAnchor, constant: 15).isActive = true
        btnScheduleAppointment.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        btnScheduleAppointment.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.width/9).isActive = true
        btnScheduleAppointment.showsTouchWhenHighlighted = true
        
        //set constraints and style upcomingAppointmentsView
        servicesScrollView.addSubview(upcomingAppointmentsView)
        upcomingAppointmentsView.translatesAutoresizingMaskIntoConstraints = false
        upcomingAppointmentsView.backgroundColor = .white
        upcomingAppointmentsView.layer.shadowOpacity = 0.1
        upcomingAppointmentsView.layer.cornerRadius = 10
        upcomingAppointmentsView.layer.shadowColor = UIColor.black.cgColor
        upcomingAppointmentsView.layer.shadowRadius = 4
        upcomingAppointmentsView.topAnchor.constraint(equalTo: btnScheduleAppointment.bottomAnchor, constant: 20).isActive = true
        upcomingAppointmentsView.centerXAnchor.constraint(equalTo: servicesScrollView.centerXAnchor).isActive = true
        upcomingAppointmentsView.leadingAnchor.constraint(equalTo: servicesScrollView.leadingAnchor, constant: 10).isActive = true
        upcomingAppointmentsView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        //set constraints for upcomingAppointmentsLabel
        upcomingAppointmentsView.addSubview(upcomingAppointmentsLabel)
        upcomingAppointmentsLabel.translatesAutoresizingMaskIntoConstraints = false
        upcomingAppointmentsLabel.text = "Upcoming Appointments"
        upcomingAppointmentsLabel.leadingAnchor.constraint(equalTo: upcomingAppointmentsView.leadingAnchor, constant: 10).isActive = true
        upcomingAppointmentsLabel.topAnchor.constraint(equalTo: upcomingAppointmentsView.topAnchor, constant: 10).isActive = true
        
        //set constraints for wrenchImage
        upcomingAppointmentsView.addSubview(wrenchImage)
        wrenchImage.translatesAutoresizingMaskIntoConstraints = false
        wrenchImage.leadingAnchor.constraint(equalTo: upcomingAppointmentsView.leadingAnchor, constant: 10).isActive = true
        wrenchImage.centerYAnchor.constraint(equalTo: upcomingAppointmentsView.centerYAnchor).isActive = true
        
        //set constraints for safetyInspectionLabel
        upcomingAppointmentsView.addSubview(safetyInspectionLabel)
        safetyInspectionLabel.translatesAutoresizingMaskIntoConstraints = false
        safetyInspectionLabel.font = UIFont(name: "Georgia", size: 16)
        safetyInspectionLabel.text = "Safety Inspection"
        safetyInspectionLabel.textAlignment = .center
        safetyInspectionLabel.leadingAnchor.constraint(equalTo: wrenchImage.trailingAnchor, constant: view.frame.width/6).isActive = true
        safetyInspectionLabel.centerYAnchor.constraint(equalTo: upcomingAppointmentsView.centerYAnchor).isActive = true
        
        //set constraints for dateLabel
        upcomingAppointmentsView.addSubview(dateLabel)
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.text = "8/21/2020"
        dateLabel.font = UIFont(name: "Baskerville", size: 13)
        dateLabel.textColor = .lightGray
        dateLabel.bottomAnchor.constraint(equalTo: wrenchImage.topAnchor, constant: -15).isActive = true
        dateLabel.leadingAnchor.constraint(equalTo: wrenchImage.leadingAnchor).isActive = true
        
        //set constraints for myStatsView
        servicesScrollView.addSubview(myStatsView)
        myStatsView.translatesAutoresizingMaskIntoConstraints = false
        myStatsView.backgroundColor = .white
        myStatsView.layer.shadowColor = UIColor.black.cgColor
        myStatsView.layer.cornerRadius = 10
        myStatsView.layer.shadowRadius = 4
        myStatsView.heightAnchor.constraint(equalToConstant: view.frame.height/1.2).isActive = true
        myStatsView.topAnchor.constraint(equalTo: upcomingAppointmentsView.bottomAnchor, constant: 20).isActive = true
        myStatsView.centerXAnchor.constraint(equalTo: servicesScrollView.centerXAnchor).isActive = true
        myStatsView.leadingAnchor.constraint(equalTo: servicesScrollView.leadingAnchor, constant: 10).isActive = true
        
        //set constraints for myStatsLabel
        myStatsView.addSubview(myStatsLabel)
        myStatsLabel.translatesAutoresizingMaskIntoConstraints = false
        myStatsLabel.text = "My Stats"
        myStatsLabel.leadingAnchor.constraint(equalTo: myStatsView.leadingAnchor, constant: 10).isActive = true
        myStatsLabel.topAnchor.constraint(equalTo: myStatsView.topAnchor, constant: 10).isActive = true
        
        //set constraints for statsStackView
        myStatsView.addSubview(statsStackView)
        statsStackView.translatesAutoresizingMaskIntoConstraints = false
        statsStackView.axis = NSLayoutConstraint.Axis.vertical
        statsStackView.distribution = .fillEqually
        statsStackView.alignment = .center
        
        thereIsWarning = false //this boolean prepares for the possibility of a warning after implementing the SDK. If there is a warning, a warning label is generated, and the myStatsView is set up slightly differently
        
        if thereIsWarning { //generates warningView; constrains warningView to top of myStatsView, above statsStackView
            //sets constraints and style for warningView
            myStatsView.addSubview(warningView)
            warningView.translatesAutoresizingMaskIntoConstraints = false
            warningView.backgroundColor = .red
            warningView.topAnchor.constraint(equalTo: myStatsLabel.bottomAnchor, constant: 5).isActive = true
            warningView.heightAnchor.constraint(equalToConstant: 50).isActive = true
            warningView.centerXAnchor.constraint(equalTo: myStatsView.centerXAnchor).isActive = true
            warningView.leadingAnchor.constraint(equalTo: myStatsView.leadingAnchor).isActive = true
    
            //set constraints for warningTextLabel
            warningView.addSubview(warningTextLabel)
            warningTextLabel.translatesAutoresizingMaskIntoConstraints = false
            warningTextLabel.text = "You are due for an inspection this month!" //needs to be made dynamic with SDK
            warningTextLabel.textColor = .white
            warningTextLabel.centerXAnchor.constraint(equalTo: warningView.centerXAnchor).isActive = true
            warningTextLabel.centerYAnchor.constraint(equalTo: warningView.centerYAnchor).isActive = true
            
            //sets constraints for warningImageView
            warningView.addSubview(warningImageView)
            warningImageView.translatesAutoresizingMaskIntoConstraints = false
            warningImageView.trailingAnchor.constraint(equalTo: warningTextLabel.leadingAnchor, constant: -4).isActive = true
            warningImageView.centerYAnchor.constraint(equalTo: warningView.centerYAnchor).isActive = true
            
            statsStackView.topAnchor.constraint(equalTo: warningView.bottomAnchor).isActive = true
        }
        else { //starts statsStackView higher on myStatsView because there is not a warning label
            statsStackView.topAnchor.constraint(equalTo: myStatsLabel.bottomAnchor, constant: 5).isActive = true
        }
        
        statsStackView.bottomAnchor.constraint(equalTo: myStatsView.bottomAnchor, constant: -15).isActive = true //bottom constraint remains the same for both if and else
        
        statsStackView.addArrangedSubview(makeItemInServicesStackView(imageName: "circleAlertGraphic", labelText: "Open Recalls", date: "test"))
        statsStackView.addArrangedSubview(makeItemInServicesStackView(imageName: "oilGraphic", labelText: "Oil Change", date: "test"))
        statsStackView.addArrangedSubview(makeItemInServicesStackView(imageName: "safetyInspectionGraphic", labelText: "Safety Inspection", date: ""))
        statsStackView.addArrangedSubview(makeItemInServicesStackView(imageName: "carGraphic", labelText: "Tire Rotation", date: ""))
        statsStackView.addArrangedSubview(makeItemInServicesStackView(imageName: "pageGraphic", labelText: "Registration", date: ""))
    }

    fileprivate func formatAccordionMenuBtns() {
        formatChatBtns(button: textChatBtn, buttonTitle: "Text Chat", overlayedImageName: "chatBubble", backgroundImageName: "textChatVisual")
        formatChatBtns(button: audioChatBtn, buttonTitle: "Audio Chat", overlayedImageName: "microphone", backgroundImageName: "audioChatVisual")
        formatChatBtns(button: videoChatBtn, buttonTitle: "Video Chat", overlayedImageName: "camera", backgroundImageName: "videoChatVisual")
        
        let upArrowImage = UIImage(named: "upTick")
        openAccordionBtn.setImage(upArrowImage, for: .normal)
        openAccordionBtn.tintColor = UIColor.gray
    }
    
    @IBAction func chatOptionsBtnTapped(_ sender: UIButton) { //animates accordion menu when opened
        chatOptionsBtns.forEach { (button) in
            UIView.animate(withDuration: 0.3) {
                button.isHidden = !button.isHidden
                self.view.layoutIfNeeded()
            }
        }
    }
    
    /// makes an item in statsStackView
    /// - Parameters:
    ///   - imageName: literal name of the image that appears on button. Image found in Assests.xcassets folder
    ///   - labelText: main text that appears on object
    ///   - date: date that appears of image
    /// - Returns: a UIView to be inserted into statsStackView
    fileprivate func makeItemInServicesStackView(imageName: String, labelText: String, date: String) -> UIView {
        let v = UIView()
        let i = UIImageView(image: UIImage(named: imageName))
        let itemLabel = UILabel(); itemLabel.text = labelText
        let dateLabel = UILabel(); dateLabel.text = date; dateLabel.textAlignment = .center; dateLabel.textColor = .lightGray
        v.translatesAutoresizingMaskIntoConstraints = false
        i.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        itemLabel.translatesAutoresizingMaskIntoConstraints = false
        v.addSubview(i)
        v.addSubview(itemLabel)
        v.addSubview(dateLabel)
        v.layer.borderWidth = 1.5
        v.layer.borderColor = UIColor(red: 0.769, green: 0.769, blue: 0.769, alpha: 1).cgColor
        i.leadingAnchor.constraint(equalTo: v.leadingAnchor, constant: 10).isActive = true
        i.centerYAnchor.constraint(equalTo: v.centerYAnchor).isActive = true
        itemLabel.leadingAnchor.constraint(equalTo: i.trailingAnchor, constant: view.frame.width/6).isActive = true
        itemLabel.centerYAnchor.constraint(equalTo: v.centerYAnchor).isActive = true
        dateLabel.bottomAnchor.constraint(equalTo: i.topAnchor, constant: -15).isActive = true
        dateLabel.leadingAnchor.constraint(equalTo: i.leadingAnchor).isActive = true
        return v
    }
    
}
